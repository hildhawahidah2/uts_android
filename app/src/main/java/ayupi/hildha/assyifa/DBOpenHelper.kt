package ayupi.hildha.assyifa

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context) : SQLiteOpenHelper(context, DB_Name, null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMurid =
            "create table murid(id text primary key, nama text not null, id_kelas int not null, id_jk int)"
        val tKelas =
            "create table kelas(id_kelas integer primary key autoincrement, nama_kelas text not null)"
        val insKelas =
            "insert into kelas(nama_kelas) values('Iqro'),('JuzAma'),('AlQuran')"
        val tJK =
            "create table jk(id_jk integer primary key autoincrement, nama_jk text not null)"
        val insJK =
            "insert into jk(nama_jk) values('Laki-Laki'),('Perempuan')"
        db?.execSQL(tMurid)
        db?.execSQL(tKelas)
        db?.execSQL(insKelas)
        db?.execSQL(tJK)
        db?.execSQL(insJK)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object {
        val DB_Name = "Assyifa3"
        val DB_Ver = 1
    }
}