package ayupi.hildha.assyifa

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.form_kelas.view.*

class Fragment_Kelas : Fragment(), View.OnClickListener {

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnInsert -> {
                builder.setTitle("Konfirmasi").setMessage("Data Yang Akan Dimasukan Sudah Benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnUpdate -> {
                builder.setTitle("Konfirmasi").setMessage("Data Akan Diupdate, Apakah Sudah Benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnDelete -> {
                builder.setTitle("Konfirmasi").setMessage("Apakah Anda Yakin Ingi Menghapus?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
        }

    }

    lateinit var thisParent: MainActivity
    lateinit var db: SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v: View
    lateinit var builder: AlertDialog.Builder
    var idKelas: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.form_kelas, container, false)
        v.btnUpdate.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsKelas.setOnItemClickListener(itemClick)

        return v
    }

    fun showDataKelas() {
        val cursor: Cursor = db.query(
            "kelas", arrayOf("nama_kelas", "id_kelas as _id"),
            null, null, null, null, "_id asc"
        )
        adapter =
            SimpleCursorAdapter(
                thisParent, R.layout.list_kelas, cursor,
                arrayOf("_id", "nama_kelas"), intArrayOf(R.id.txIdKelas, R.id.txNamaKelas),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
            ) as ListAdapter
        v.lsKelas.adapter = adapter

    }

    override fun onStart() {
        super.onStart()
        showDataKelas()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idKelas = c.getString(c.getColumnIndex("_id"))
        v.edNamaKelas.setText(c.getString(c.getColumnIndex("nama_kelas")))
    }

    fun insertDataKelas(namaKelas: String) {
        var cv: ContentValues = ContentValues()
        cv.put("nama_kelas", namaKelas)
        db.insert("kelas", null, cv)
        showDataKelas()
    }

    fun updateDataKelas(namaKelas: String, idKelas: String) {
        var cv: ContentValues = ContentValues()
        cv.put("nama_kelas", namaKelas)
        db.update("kelas", cv, "id_kelas = $idKelas", null)
        showDataKelas()
    }

    fun deleteDataKelas(idKelas: String) {
        db.delete("kelas", "id_kelas = $idKelas", null)
        showDataKelas()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataKelas(v.edNamaKelas.text.toString())
        v.edNamaKelas.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataKelas(v.edNamaKelas.text.toString(), idKelas)
        v.edNamaKelas.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataKelas(idKelas)
        v.edNamaKelas.setText("")
    }


}