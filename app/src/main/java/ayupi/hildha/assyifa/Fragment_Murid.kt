package ayupi.hildha.assyifa

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.form_murid.*
import kotlinx.android.synthetic.main.form_murid.view.*
import kotlin.random.Random

class Fragment_Murid : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {

        spinner.setSelection(1, true)
        spinner2.setSelection(1, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaKelas = c.getString(c.getColumnIndex("_id"))
        val d: Cursor = spAdapter1.getItem(position) as Cursor
        namaJK = d.getString(d.getColumnIndex("_id"))


    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.btnInsert -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah Data yang adna masukan sudah benar?")
                    .setPositiveButton("ya", btnInsertDialog)
                    .setNegativeButton("tidak", null)
                dialog.show()
            }
            R.id.btnDelete -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data  akan di hapus ?")
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnUpdate -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang di edit sudah benar?")
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()

            }

            R.id.btnSearch -> {
                showDataMurid(edNamaMurid.text.toString())
            }

        }

    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var spAdapter1: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var namaMurid: String = ""
    var namaKelas: String = ""
    var namaJK: String = ""
    var noMurid: String = ""
    lateinit var db: SQLiteDatabase
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.form_murid, container, false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)

        v.btnDelete.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnUpdate.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.spinner2.onItemSelectedListener = this
        v.btnSearch.setOnClickListener(this)
        v.lsMurid.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMurid("")
        showDataKelas()
        showDataJK()
        edNoMurid.setText(getDataRandom(3))
    }

    fun getDataRandom(length: Int): String {
        val sb = StringBuilder(length)
        val random = "abcdefghijklmnopqrstuvwxyz1234567890"
        val rand = Random
        for (i in 0 until sb.capacity()) {

            val index = rand.nextInt(random.length)
            sb.append(random[index])
        }
        return sb.toString()
    }

    fun showDataMurid(namaMurid: String) {
        var sql = ""
        if (!namaMurid.trim().equals("")) {
            sql =
                "select m.id as _id, m.nama, j.id_jk, j.nama_jk, k.id_kelas, k.nama_kelas from murid m, kelas k, jk j " +
                        "where m.id_kelas = k.id_kelas and m.id_jk = j.id_jk and m.nama like '%$namaMurid%'"
        } else {
            sql =
                "select m.id as _id, m.nama, j.id_jk, j.nama_jk, k.id_kelas, k.nama_kelas from murid m, kelas k, jk j " +
                        "where m.id_kelas = k.id_kelas and m.id_jk = j.id_jk order by m.nama asc"
        }
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent,
            R.layout.list_murid,
            c,
            arrayOf("_id", "nama", "nama_kelas", "nama_jk"),
            intArrayOf(R.id.txNoMurid, R.id.txNamaMurid, R.id.txNamaKelas, R.id.txNamaJK),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsMurid.adapter = lsAdapter
    }

    fun showDataKelas() {
        val c: Cursor =
            db.rawQuery("select nama_kelas as _id from kelas order by nama_kelas asc", null)
        spAdapter = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun showDataJK() {
        val d: Cursor =
            db.rawQuery("select nama_jk as _id from jk order by nama_jk asc", null)
        spAdapter1 = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            d,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner2.adapter = spAdapter1
        v.spinner2.setSelection(0)
    }


    fun insertDataMurid(id: String, namaMurid: String, id_kelas: Int, id_jk: Int) {
        var sql = "insert into murid(id, nama, id_kelas, id_jk) values (?,?,?,?)"
        db.execSQL(sql, arrayOf(id, namaMurid, id_kelas, id_jk))
        showDataMurid("")
    }

    fun deleteDataMurid(id: String) {
        db.delete("murid", "id = '$id'", null)
        showDataMurid("")
    }

    fun updateDataMurid(id: String, namaMurid: String, id_kelas: Int, id_jk: Int) {
        var cv: ContentValues = ContentValues()
        cv.put("id", id)
        cv.put("nama", namaMurid)
        cv.put("id_kelas", id_kelas)
        cv.put("id_jk", id_jk)
        db.update("murid", cv, "id = '$noMurid'", null)
        showDataMurid("")
    }


    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kelas from kelas where nama_kelas='$namaKelas'"
        val c: Cursor = db.rawQuery(sql, null)
        var sql1 = "select id_jk from jk where nama_jk='$namaJK'"
        val d: Cursor = db.rawQuery(sql1, null)

        if (c.count > 0 && d.count > 0) {
            c.moveToFirst()
            d.moveToFirst()
            insertDataMurid(
                v.edNoMurid.text.toString(), v.edNamaMurid.text.toString(),
                c.getInt(c.getColumnIndex("id_kelas")),
                d.getInt(d.getColumnIndex("id_jk"))
            )
            v.edNoMurid.setText(getDataRandom(3))
            v.edNamaMurid.setText("")
            v.spinner2.setSelection(0)
            v.spinner.setSelection(0)

        }
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMurid(noMurid)
        v.edNamaMurid.setText("")
        v.edNoMurid.setText(getDataRandom(3))
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kelas from kelas where nama_kelas='$namaKelas'"
        val c: Cursor = db.rawQuery(sql, null)
        var sql1 = "select id_jk from jk where nama_jk='$namaJK'"
        val d: Cursor = db.rawQuery(sql1, null)

        if (c.count > 0 && d.count > 0) {
            c.moveToFirst()
            d.moveToFirst()
            updateDataMurid(
                v.edNoMurid.text.toString(), v.edNamaMurid.text.toString(),
                c.getInt(c.getColumnIndex("id_kelas")),
                d.getInt(d.getColumnIndex("id_jk"))
            )
            v.edNoMurid.setText(getDataRandom(3))
            v.edNamaMurid.setText("")
            v.spinner2.setSelection(0)
            v.spinner.setSelection(0)

        }
    }


    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        noMurid = c.getString(c.getColumnIndex("_id"))
        v.edNoMurid.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaMurid.setText(c.getString(c.getColumnIndex("nama")))
        v.spinner.setSelection(c.getInt(c.getColumnIndex("nama_kelas")))
        v.spinner2.setSelection(c.getInt(c.getColumnIndex("nama_jk")))

    }
}
